﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net.Http;
using HtmlAgilityPack;

namespace commander_dashboard
{

    
    public partial class Form1 : Form
    {
        private Timer timer1;
        private string previous_jump = "";
        private string API = "656vepmfld0k0osw8gg4owco8c8ggw0g40k0sow";

        public Form1()
        {
            InitializeComponent();
            Properties.Settings.Default.log_folder = "%USERPROFILE%\\Saved Games\\Frontier Developments\\Elite Dangerous";
            Properties.Settings.Default.Save();
            InitTimer();
            
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            settings setting = new settings();
            setting.ShowDialog();
        }


        public void InitTimer()
        {
            timer1 = new Timer();
            timer1.Tick += new EventHandler(timer1_Tick);
            timer1.Interval = 2000; // in miliseconds
            timer1.Start();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            get_FSDJump(Properties.Settings.Default.log_folder);
        }


        private void get_FSDJump(string path)
        {

            var filePath = Environment.ExpandEnvironmentVariables(path);
            var directory = new DirectoryInfo(filePath);
            string file = "";
            try
            {
                // On récupère le dernier fichier log du dossier
                var myFile = (from f in directory.GetFiles("*.log")
                              orderby f.LastWriteTime descending
                              select f).First();
                file = myFile.ToString();
            }
            catch
            {
                lbl_test.Text = "ERROR cannot read log file";
            }

            string fullPath = directory + "\\" + file;
            string lastJump = string.Empty;


            using (FileStream fs = new FileStream(fullPath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
            {
                using (StreamReader sr = new StreamReader(fs))
                {
                    string all = sr.ReadToEnd();
                    string[] lines = all.Split('\n');
                    foreach (string line in lines)
                    {
                        if (line.Contains("FSDJump"))
                        {
                            lastJump = line;

                        }
                    }
                }
            }

            if (String.Equals(lastJump, previous_jump))
            {
                // n'a pas changé de système
            }
            else
            {
                if (string.IsNullOrEmpty(lastJump))
                {
                    // pas de saut FSD
                }
                else
                {
                    // Saut FSD detecté
                    get_system(lastJump);
                }
                
            }
            previous_jump = lastJump;
            
        }

        private void get_system(string jump)
        {
            dynamic stuff1 = Newtonsoft.Json.JsonConvert.DeserializeObject(jump);
            string system = stuff1.StarSystem;
            get_trade_station(system);
        }

        private async void get_trade_station(string system)
        {

            lbl_system.Text = system;
            string Estation = "";
            string Esystem_station = "";
            string Etype = "";
            string Fstation = "";
            string Fsystem_station = "";
            string Ftype = "";
            string Bstation = "";
            string Bsystem_station = "";
            string Btype = "";
            string type1 = "Encodé";
            string type2 = "Manufacturé";
            string type3 = "Pur";
            int i = 1;
            int j = 1;
            int k = 1;


            // Matériaux encodés
            do
            {
                
                var html = @"https://www.edsm.net/fr/search/stations/index/cmdrPosition/" + system + "/economy/4/service/71/sortBy/distanceCMDR";
                HtmlWeb web = new HtmlWeb();
                var htmlDoc = web.Load(html);
                var nodes = htmlDoc.DocumentNode.SelectNodes("/html/body/main/div[3]/div/div/div/table/tbody");
                foreach (var node in nodes)
                {

                    Estation = node.SelectSingleNode("tr["+i+"]/td[2]/a/strong").InnerText;
                    Esystem_station = node.SelectSingleNode("tr[" + i + "]/td[2]/small/a").InnerText;
                    Etype = node.SelectSingleNode("tr[" + i + "]/td[7]").InnerText;
                }
                i++;
                // On sécurise la boucle infini
                if (i == 20)
                {
                    break;
                }
            }
            while (!Etype.Contains(type1));
            lbl_station_encode.Text = Estation;
            lbl_system_encode.Text = Esystem_station;

            // Matériaux fabriqué
            do
            {
                
                var html = @"https://www.edsm.net/fr/search/stations/index/cmdrPosition/" + system + "/economy/5/service/71/sortBy/distanceCMDR";
                HtmlWeb web = new HtmlWeb();
                var htmlDoc = web.Load(html);
                var nodes = htmlDoc.DocumentNode.SelectNodes("/html/body/main/div[3]/div/div/div/table/tbody");
                foreach (var node in nodes)
                {

                    Fstation = node.SelectSingleNode("tr[" + j + "]/td[2]/a/strong").InnerText;
                    Fsystem_station = node.SelectSingleNode("tr[" + j + "]/td[2]/small/a").InnerText;
                    Ftype = node.SelectSingleNode("tr[" + j + "]/td[7]").InnerText;
                }
                j++;
                // On sécurise la boucle infini
                if (j == 20)
                {
                    break;
                }
            }
            while (!Ftype.Contains(type2));
            lbl_fabrique_station.Text = Fstation;
            lbl_fabrique_system.Text = Fsystem_station;


            // Matériaux brut
            do
            {
                
                var html = @"https://www.edsm.net/fr/search/stations/index/cmdrPosition/" + system + "/economy/3/service/71/sortBy/distanceCMDR";
                HtmlWeb web = new HtmlWeb();
                var htmlDoc = web.Load(html);
                var nodes = htmlDoc.DocumentNode.SelectNodes("/html/body/main/div[3]/div/div/div/table/tbody");
                foreach (var node in nodes)
                {

                    Bstation = node.SelectSingleNode("tr[" + k + "]/td[2]/a/strong").InnerText;
                    Bsystem_station = node.SelectSingleNode("tr[" + k + "]/td[2]/small/a").InnerText;
                    Btype = node.SelectSingleNode("tr[" + k + "]/td[7]").InnerText;
                }
                k++;
                if (k == 20)
                {
                    break;
                }

            }
            while (!Btype.Contains(type3));
            lbl_brut_station.Text = Bstation;
            lbl_brut_system.Text = Bsystem_station;


        }

    }
}
