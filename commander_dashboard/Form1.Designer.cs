﻿
namespace commander_dashboard
{
    partial class Form1
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.lbl_test = new System.Windows.Forms.Label();
            this.lbl_system = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.lbl_station_encode = new System.Windows.Forms.Label();
            this.lbl_system_encode = new System.Windows.Forms.Label();
            this.lbl_fabrique_station = new System.Windows.Forms.Label();
            this.lbl_fabrique_system = new System.Windows.Forms.Label();
            this.lbl_brut_station = new System.Windows.Forms.Label();
            this.lbl_brut_system = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label6 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Cascadia Code", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(229, 31);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(386, 25);
            this.label1.TabIndex = 0;
            this.label1.Text = "Talyov Army : Commmander dashboard";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(819, 12);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 2;
            this.button1.Text = "Settings";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // lbl_test
            // 
            this.lbl_test.AutoSize = true;
            this.lbl_test.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_test.ForeColor = System.Drawing.Color.Red;
            this.lbl_test.Location = new System.Drawing.Point(10, 496);
            this.lbl_test.Name = "lbl_test";
            this.lbl_test.Size = new System.Drawing.Size(0, 24);
            this.lbl_test.TabIndex = 3;
            // 
            // lbl_system
            // 
            this.lbl_system.AutoSize = true;
            this.lbl_system.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_system.ForeColor = System.Drawing.Color.Lime;
            this.lbl_system.Location = new System.Drawing.Point(85, 39);
            this.lbl_system.Name = "lbl_system";
            this.lbl_system.Size = new System.Drawing.Size(36, 20);
            this.lbl_system.TabIndex = 4;
            this.lbl_system.Text = "???";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(307, 213);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(61, 16);
            this.label2.TabIndex = 5;
            this.label2.Text = "Encodé";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(31, 213);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(35, 16);
            this.label3.TabIndex = 6;
            this.label3.Text = "Brut";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(156, 213);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(70, 16);
            this.label4.TabIndex = 7;
            this.label4.Text = "Fabriqué";
            // 
            // lbl_station_encode
            // 
            this.lbl_station_encode.AutoSize = true;
            this.lbl_station_encode.Location = new System.Drawing.Point(307, 252);
            this.lbl_station_encode.Name = "lbl_station_encode";
            this.lbl_station_encode.Size = new System.Drawing.Size(25, 13);
            this.lbl_station_encode.TabIndex = 8;
            this.lbl_station_encode.Text = "???";
            // 
            // lbl_system_encode
            // 
            this.lbl_system_encode.AutoSize = true;
            this.lbl_system_encode.Location = new System.Drawing.Point(307, 297);
            this.lbl_system_encode.Name = "lbl_system_encode";
            this.lbl_system_encode.Size = new System.Drawing.Size(25, 13);
            this.lbl_system_encode.TabIndex = 9;
            this.lbl_system_encode.Text = "???";
            // 
            // lbl_fabrique_station
            // 
            this.lbl_fabrique_station.AutoSize = true;
            this.lbl_fabrique_station.Location = new System.Drawing.Point(156, 252);
            this.lbl_fabrique_station.Name = "lbl_fabrique_station";
            this.lbl_fabrique_station.Size = new System.Drawing.Size(25, 13);
            this.lbl_fabrique_station.TabIndex = 10;
            this.lbl_fabrique_station.Text = "???";
            // 
            // lbl_fabrique_system
            // 
            this.lbl_fabrique_system.AutoSize = true;
            this.lbl_fabrique_system.Location = new System.Drawing.Point(156, 297);
            this.lbl_fabrique_system.Name = "lbl_fabrique_system";
            this.lbl_fabrique_system.Size = new System.Drawing.Size(25, 13);
            this.lbl_fabrique_system.TabIndex = 11;
            this.lbl_fabrique_system.Text = "???";
            // 
            // lbl_brut_station
            // 
            this.lbl_brut_station.AutoSize = true;
            this.lbl_brut_station.Location = new System.Drawing.Point(31, 252);
            this.lbl_brut_station.Name = "lbl_brut_station";
            this.lbl_brut_station.Size = new System.Drawing.Size(25, 13);
            this.lbl_brut_station.TabIndex = 12;
            this.lbl_brut_station.Text = "???";
            // 
            // lbl_brut_system
            // 
            this.lbl_brut_system.AutoSize = true;
            this.lbl_brut_system.Location = new System.Drawing.Point(31, 297);
            this.lbl_brut_system.Name = "lbl_brut_system";
            this.lbl_brut_system.Size = new System.Drawing.Size(25, 13);
            this.lbl_brut_system.TabIndex = 13;
            this.lbl_brut_system.Text = "???";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Cascadia Code", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(67, 153);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(289, 21);
            this.label5.TabIndex = 14;
            this.label5.Text = "Trade stations les plus proches";
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Location = new System.Drawing.Point(14, 128);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(386, 219);
            this.panel1.TabIndex = 15;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(13, 42);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(67, 16);
            this.label6.TabIndex = 16;
            this.label6.Text = "Système :";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(906, 527);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.lbl_brut_system);
            this.Controls.Add(this.lbl_brut_station);
            this.Controls.Add(this.lbl_fabrique_system);
            this.Controls.Add(this.lbl_fabrique_station);
            this.Controls.Add(this.lbl_system_encode);
            this.Controls.Add(this.lbl_station_encode);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.lbl_system);
            this.Controls.Add(this.lbl_test);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.panel1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label lbl_test;
        private System.Windows.Forms.Label lbl_system;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label lbl_station_encode;
        private System.Windows.Forms.Label lbl_system_encode;
        private System.Windows.Forms.Label lbl_fabrique_station;
        private System.Windows.Forms.Label lbl_fabrique_system;
        private System.Windows.Forms.Label lbl_brut_station;
        private System.Windows.Forms.Label lbl_brut_system;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label6;
    }
}

