﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;


namespace commander_dashboard
{
    public partial class settings : Form
    {
        public settings()
        {
            InitializeComponent();
            lbl_warning.Hide();
            folder.Text = Properties.Settings.Default.log_folder;
            Properties.Settings.Default.Save();
            
        }

        private void button1_Click(object sender, EventArgs e)
        {

            string path = folder.Text;
            string filePath = Environment.ExpandEnvironmentVariables(path);
            if (Directory.Exists(filePath))
            {
                Properties.Settings.Default.log_folder = filePath;
                Properties.Settings.Default.Save();
                this.Close();
            }
            else
            {
                lbl_warning.Show();
            }
        }
    }
}
